
// Перо-наперво узнаем id книги, которую будем показывать
var bookId = window.location.search.replace('?', '');

var bookData;

// Функция для подставления данных на страницу карточки книги
function takeBook(data){
	bookData = data;
	// console.log(bookData);
	var marker = takeMarker();
	$('.book-marker').addClass(marker);
	$('.book-cover img').attr({
		'src': bookData.cover.large
	});
	$('.book-description').html(bookData.description);
	for (var i = 0; i < 2; i++) {
		$($('.reviews .avatar img')[i]).attr({
			'src': bookData.reviews[i].author.pic,
			'alt': bookData.reviews[i].author.name
		});
		$($('.reviews .cite')[i]).html(bookData.reviews[i].cite);
		$($('.features .avatar img')[i]).attr({
			'src': bookData.features[i].pic
		});
		$($('.features .cite')[i]).html(bookData.features[i].title);
	}
	$('button.buy span#price').text(bookData.price);
	citeHeights();
	$('.card').css('opacity', '1');
};

// Чтобы все было ровненько!
function citeHeights(){
	var heights = [];
	$(".cite").each(function(indx, element){
  		heights.push($(element).height());
	});	
	var max = Math.max.apply(0, heights);
	$(".cite").height(max);
}

ajax(takeBook, bookId);

// Когда все загрузилось
$(function(){
	$('button.buy').click(function() {
		window.location = '../order/' + "?" + bookId;
	});
	
	$('#coverimg')[0].onload = function() {
		eyeMoove();
	};
});

