
// Функция первоначального добавления книг на страницу
function takeBooks(data){
	booksData = data;
	var books = $('.book');
	var marker;
	var cookieBooks = $.cookie('books');
	if (cookieBooks && cookieBooks != 'null'){
		cookieBooks = JSON.parse(cookieBooks);
		if (cookieBooks[0].id){
			if (cookieBooks.length > books.length){
				for (var i = cookieBooks.length - books.length - 1; i >= 0; i--) {
					$(books[0]).clone().insertBefore($('#books-sort').find('.cl'));
				}
				if (cookieBooks.length == booksData.length){
					$('.books-more').addClass('locked');
				}
				books = $('.book');
			}
			books.each(function(i, el) {
				marker = takeMarker();
				el.id = cookieBooks[i].id;
				$(el).find('.book-marker').addClass(marker);
			});
			booksData.forEach(function(el){
				var thisBook = $('#'+el.id);
				thisBook.find('img').attr('src', el.cover.small);
				thisBook.find('.book-info').html(el.info);
			})
			books.fadeIn(300);
			$('.books-more').fadeIn(300);
			return
		}
	}
	
	for (var i = books.length - 1; i >= 0; i--) {
		marker = takeMarker();
		$(books[i]).attr({
			'id': booksData[i].id
		});
		$(books[i]).find('img').attr({
			'src': booksData[i].cover.small
		});
		$(books[i]).find('.book-info').html(booksData[i].info);
		$(books[i]).find('.book-marker').addClass(marker);
	}
	books.fadeIn(300);
	$('.books-more').fadeIn(300);
};

// Функция добавления новой книжки
function newBook(i, n){
	var books = $('.book');
	var marker = takeMarker();
	var newBook = $(books[0]).clone();
	$(newBook).attr({
		'id': booksData[n + i].id
	});
	$(newBook).find('img').attr({
		'src': booksData[n + i].cover.small
	});
	$(newBook).find('.book-info').html(booksData[n + i].info);
	$(newBook).find('.book-marker').addClass(marker);
	$(newBook).click(function(){
		clickBooks(this);
	});
	$(newBook).insertBefore($('#books-sort').find('.cl'));
};

// Функция загрузки карточки книги по клику на книгу в витрине
function clickBooks(book){
	saveBooks();
  	window.location = 'book/?' + book.id;
}

// Сохраняем состояние витрины в куки
function saveBooks(){
	var books = $('.book');
	var booksNow = [];
	books.each(function(index, el) {
		var book = {
			'id': el.id,
		};
		booksNow.push(book);
	});
	var booksSave = JSON.stringify(booksNow);
	$.cookie('books', booksSave);
}

// Делаем запрос на все книги (без id) и вызываем функцию takeBooks
ajax(takeBooks);

// Когда все загрузилось
$(function(){
	// Поставим на кнопку "еще несколько книг" обработчик
	$('.books-more').click(function() {
		var books = $('.book');
		if (books.length == booksData.length){
			return;
		}else{
			if (booksData.length - books.length > 4){
				for (var i = 0; i <= 3; i++) {
					newBook(i, books.length)
				}
			}else{
				for (var i = 0; i < booksData.length - books.length; i++) {
					newBook(i, books.length)
				}
				$(this).addClass('locked');
			}
		}
	});

	$('.book').click(function(){
		clickBooks(this);
	});

	$('#books-sort').sortable({
		 placeholder: 'lineSpace',
		 helper: 'clone',
		  start: function(event, ui){
		  	ui.placeholder.prev('.book').before('<div class = "book" id = "book-empty"></div>');
		  	ui.placeholder.css('display', 'none');
		 },
		 change: function(event, ui){
		 	if (ui.placeholder.prev()[0] == ui.helper[0] || ui.placeholder.next()[0] == ui.helper[0]|| 
		 		ui.placeholder.prev()[0] == $('#book-empty')[0] || ui.placeholder.next()[0] == $('#book-empty')[0]){
		 		ui.placeholder.css('display', 'none');
		 	}else{
		 		ui.placeholder.css('display', 'block');
		 	}
		 },
		 stop: function(){
		 	$('#book-empty').remove();
		 }
	});

	$('footer a').click(function(e) {
		e.preventDefault();
		saveBooks();
		window.location = this.href;
	});

});

