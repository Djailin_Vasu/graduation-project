
// Когда все загрузилось
$(function() {
	// Для начала поставим проверки во время ввода
	$('input[type = text][name != search]').keyup(function(e){
		$(this).prev('p.error').css('display', 'none');
		$(this).removeClass('error');
		if (this.name == 'name') {// Для ввода имени
			if (this.value.match(/[^а-яёa-z -]/ig)){
  			     this.value = this.value.replace(/[^а-яёa-z -]/ig, '');
  			     $(this).prev('p.error').css('display', 'block').text('Пожалуйста, только буквы!');
  			 }
		}else if(this.name == 'phone'){ //для ввода телефона
			if (this.value.match(/[^0-9\+ -/)/)]/g)){
				 this.value = this.value.replace(/[^0-9\+ -/)/)]/g, '');
				 $(this).prev('p.error').css('display', 'block').text('Мы понимаем номера телефонов только в цифрах!');
			}
  			if (this.value.search(/^[ -\(\)]/g) != -1){
  				this.value = this.value.replace(/^[ -\(\)]/g, function(str, offset){
  					if (offset == 0) {
  						return '';
  					}else{
  						return str;
  					}
  				});
  				// console.log('j')
  			};
  			if (this.value.search(/\+/g) != -1) {
  					this.value = this.value.replace(/\+/g, function (str, offset) {
  						if (offset == 0) {
  							return str;
  						}else{
  							return '';
  						}
  					});
  			}
		}else if (this.name == 'e-mail'){ //для ввода e-mail
			if (this.value.match(/[а-яё]/ig)){
				this.value = this.value.replace(/[а-яё]/i, '');
				$(this).prev('p.error').css('display', 'block').text('Мы не верим в существования элекронных адресов на русском!');
			}
		}
	});
	// Когда поле уже заполнено
	$('input[type = text][name != search]').change(function(e) {
		$(this).removeClass('error')
		if (this.value == '') {
			$(this).prev('p.error').css('display', 'block').text('Не проходите мимо, не оставляйте это поле пустым!');
		}else{
			if (this.name == 'phone'){
				if (!/^[\d+][\d\(\)\ -]{6,14}\d$/.test(this.value)){
					$(this).prev('p.error').css('display', 'block').text('С этим номером что-то не так.. О_о');
				}else{
					$(this).prev('p.error').css('display', 'none');
				}
			}else if(this.name == 'e-mail'){
				if(!/^[\w-\.]+@[\w-]+\.[a-z]{2,3}$/i.test(this.value)){
					$(this).prev('p.error').css('display', 'block').text('А это точно электронный адрес?');
				}else {
					$(this).prev('p.error').css('display', 'none');
				}
			}else{
				$(this).prev('p.error').css('display', 'none');
			}
		}

	});

    // блокируем нажание enter на форме
    $('form').bind('keydown', function (e) {
          if (e.keyCode === 13) {
            e.preventDefault();
            
        }
    });

	$('button.order-make').click(function(e) {
		e.preventDefault();
		var result = true;
		$('input[type = text][name != search]').each(function(index, el) {
			if (el.value == ''){
				if (el.name == 'adress' && $(el).parent('.order-adress').css('display') == 'block'){
					$(el).prev('p.error').css('display', 'block').text('А куда доставлять-то?');
					$(el).addClass('error');
					result = false;
				}else if (el.name != 'adress'){
					$(el).prev('p.error').css('display', 'block').text('Заполните это поле');
					$(el).addClass('error');
					result = false;
				}
			}else if($(el).prev('p.error').css('display') == 'block'){
				$(el).addClass('error');
				result = false;
			}
		});
		if(!$('input[name = delivery]:checked').val()){
			$('fieldset.delivery').children('p.error').css('display', 'block').text('Определитесь, пожалуйста');
			result = false;
		}
		if(!$('input[name = payment]:checked').val()){
			$('fieldset.payment').children('p.error').css('display', 'block').text('А как будем расплачиваться?');
			result = false;
		}
		if (result){
			submitForm();
		}		
	});
});