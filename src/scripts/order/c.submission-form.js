function submitForm() {
	$('form.order').css('display', 'none');
	$('.preloader').css('display', 'block');
	var comment = $('fieldset.comment textarea').val();
	if (comment == ''){
		comment = "none"
	};

	 var body = {
		'manager': 'djailin.vasu@gmail.com',
		'book': bookId,
		'name': $('input[name = name]').val(),
		'phone': $('input[name = phone]').val(),
		'email': $('input[name = e-mail]').val(),
		'comment': comment,
		'delivery': {
		  'id': $('input[name = delivery]:checked')[0].id,
		  'address': $('input[name = adress]').val(),
		},
		'payment': {
		  'id': $('input[name = payment]:checked')[0].id,
		  'currency': 'R01235'
		}
	};
	$.ajax({
		url: 'https://netology-fbb-store-api.herokuapp.com/order',
		type: 'POST',
		processData: false,
		data: JSON.stringify(body),
		contentType: 'application/json',
		success: function (){
			$('.preloader').css('display', 'none');
			$('.done').css('display', 'block');
		},
		error: function (r, type) {
			$('.preloader').css('display', 'none');
			$('form.order').css('display', 'block');
			$('.error.g').css('display', 'block').text(function () {
				if (type == 'timeout') {
					return 'Кажется, наш почтовый ящик засорился. Пожалуйста, попробуйте позже.';
				}else{
					return 'Упс. Что-то пошло не так. Проверьте поля и попробуйте еще раз!';
				}
			});

		}
	});
}