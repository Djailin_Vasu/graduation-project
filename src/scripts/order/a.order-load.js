// Узнаем ID книги, которую у нас хотят купить
var bookId = window.location.search.replace('?', '');

var bookData;
var delivery;
var payment;

// Загрузим данные книги на страницу
function loadBook(data) {
	bookData = data;
	$('h2.order-for a').text(bookData.title).attr({
		'href': '../book/?' + bookId
	});

	var marker = takeMarker();
	$('.book-marker').addClass(marker);

	$('.book-cover img').attr({
		'src': bookData.cover.large
	});

	$('#priceres').text(bookData.price).data({'priceBook': bookData.price, 'priceDel': 0});

	$('.book').click(function() {
		window.location = '../book/?' + bookId;
	}).css('opacity', '1');
};

// Загурзим данные способов доставки на страницу
function getDelivery(data){
	delivery = data;

	delivery.forEach(function(elem){
		var radioBox = document.createElement('p');

		$(radioBox).html('<input class="radio" type="radio" name="delivery"><label></label>')
		.find('input').attr({
			'id': elem.id,
			'value': elem.price
		}).data('needAdress', elem.needAdress).change(function() {
			radioDelivery(this);
		});
		if (elem.price == 0){
			$(radioBox).find('label').html(elem.name + " - Бесплатно");
		}else{
			$(radioBox).find('label').html(elem.name +" - "+ elem.price + "$");
		}
		$(radioBox).find('label').attr({
			'for': elem.id
		});
		$(radioBox).appendTo('fieldset.delivery');
	});
}

// Загурзим способы оплаты на страницу
function getPayment(data){
	payment = data;
	payment.forEach(function (elem) {
		var radioBox = document.createElement('p');
		$(radioBox).html('<input class="radio" type="radio" name="payment"><label></label>')
		.find('input').attr({
			'id': elem.id,
			'value': elem.title,
			'disabled':''
		}).data('deliverys', elem.availableFor).change(function() {
			$('fieldset.payment').children('p.error').css('display', 'none');
		});
		$(radioBox).find('label').attr({
			'for': elem.id
		}).text(elem.title);
		$(radioBox).appendTo('fieldset.payment');
	});
}

// Что должно происходить при выборе способов доставки
function radioDelivery(radio){
	$('fieldset.delivery').children('p.error').css('display', 'none');
	$('.order-adress').css('display', 'none');
	if ($(radio).data('needAdress')){
		$('.order-adress').detach().insertAfter($(radio).parent('p'));
		$('.order-adress').css('display', 'block');
	}
	$('#priceres').data('priceDel', +radio.value)
	
	$('#priceres').text($('#priceres').data('priceDel') + $('#priceres').data('priceBook'));
	$('fieldset.payment input').each(function(i, el) {
		var deliverys = $(el).data('deliverys');
		if (deliverys.includes(radio.id)){
			$(el).removeAttr('disabled');
		}else{
			$(el).attr('disabled', '');
			el.checked = false;
		}
	});
}

// Попросим данные о книгах у сервера и запустим функцию обработки их
ajax(loadBook, bookId);
// Попросим данные о способах доставки у сервера и запустим функцию обработки их
$.get('https://netology-fbb-store-api.herokuapp.com/order/delivery', getDelivery);
// Попросим даннные о способах оплаты у сервера и запустим функцию обработки их
$.get('https://netology-fbb-store-api.herokuapp.com/order/payment', getPayment);