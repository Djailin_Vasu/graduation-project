// Тут будем хранить данные книг
var booksData;

function ajax(doIt, id){
	if(id == undefined){
		id = "";
	}
    $.ajax({
    url: "https://netology-fbb-store-api.herokuapp.com/book/" + id,
    success: function(data){
      	doIt(data);
    },
    error: function() {
    	$('.messege').addClass('error').text('Упс! Кажется, наш генератор случайных изданий перегрелся. Пожалуйста, попробуйте подключиться позже.')
    }
  });
};

// Функция определения цвета закладочки у книги, потому что ку
function takeMarker(){
	var random;
	random = Math.floor(Math.random() * (3 - 1 + 1)) + 1;
	if (random == 1){
		return '';
	}else if(random == 2){
		return 'red';
	}else{
		return 'green';
	}
};

function simpleTake(data){
	booksData = data;
	search()
}

function search(){
	$('.search-result .search-book').remove();
	var value = $('form.search input[type = text]').val();
	var result = [];
	var href = "../book/?";
	var reg = new RegExp(value, 'gi');
	booksData.forEach(function(el){
		if (el.author.name.search(reg) != -1 || el.title.search(reg) != -1){
			result.push(el);
		}
	});
	if (window.location.pathname == '/'){
		href = "book/?";
	}
	if (result.length == 0){
		$('.search-message').text('Ничего не найдено. Совсем ничего.').addClass('error');
	}else{
		$('.search-message').text('Вот что мы нашли.').removeClass('error');
		result.forEach(function(el){
			$('.search-message').append('<p class="search-book">' + el.author.name + ': <a href="' + href + el.id +'">' + el.title + '</a></p>');
		});
	}
	$('.search-result').fadeIn(300);
}

$(function(){
	$('form.search input[type = submit]').click(function(e) {
		e.preventDefault();
		if ($('form.search input[type = text]').val() != ''){
			if (!booksData){
			ajax(simpleTake);
				return;
			}else{
				search()
			}
		}else{
			$('.search-result').fadeOut(300);
			$('.search-result .search-book').remove();
		}
	});
	$('form.search input[type = text]').bind('keydown', function (e) {
          if (e.keyCode === 13) {
            $('form.search input[type = submit]').click();
        }
    });
	$('.search-close').click(function() {
		$(this).parent('.search-result').fadeOut(300);
		$('.search-result .search-book').remove();
		$('form.search input[type = text]').val('');
	});
});