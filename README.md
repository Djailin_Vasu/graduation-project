# Как проект устроен  

##Посмотреть результат
Можно тут : <http://university.netology.ru/user_data/denisova/fbb-store/>  

***
##Инструменты разработки
Используется препроцессор less css.  

Для простановки префиксов в стилях использован Автопрефиксер - <https://github.com/postcss/autoprefixer>.  

Для оптимизации изображений использован минизизатор Imagemin - <https://github.com/gruntjs/grunt-contrib-imagemin>.  

Для реализации перетаскивания книг в витрине мышкой использован плагин Sortable из набора jQuery UI - <http://jqueryui.com/>.
 
Для реализации запоминания состояния витрины на главной страницы использован плагин jQuery Cookie - <https://plugins.jquery.com/cookie/>

----------
##Описание файлов
1. Папка **/result/** - итоговые файлы проекта

    * **index.html** - *разметка главной страницы*

    * **/about/index.html** - *разметка страницы о издательстве*

    * **/book/index.html** - *разметка страницы карточки книги*

    - **/css/main.css** - *собранные в одну красивую кучку стили (спасибо, grunt-contrib-concat)*

    - **/font/alsfuchsia.otf** - *наш специальный декоративный шрифт* 

    - **/images/** - *папка с картинками, все сжато в лучших традициях!* 

    - **/js/script.js** - *общие для страниц функции js* 

    - **/js/main.js** - *Только тот код, который нужен только на главной странице* 

    - **/js/book.js** - *Только тот код, который нужен только на странице карточки книги* 

    - **/js/order.js** - *Только тот код, который нужен только на странице оформления заказа* 

    * **/order/index.html** - *разметка страницы заказа*

1. Папка **/src/** - собственно, сама кухня. Я имею ввиду, конечно, исходники!

    - **/images/** - *исходные картинки, они не сжаты и никуда не годятся, но приятно, что они есть*

    - **/scripts/** - *папочка с исходными js-файлами*

        - **overal.js** - *функции, которые нужны везде или хотя бы на 2х страницах*

        - **jquery-3.0.0.min.js** - *библиотека jQuery*

        - **main/take-books.js** - *Скрипты для главной страницы*

        - **main/jquery-ui.min.js** - *Плагин Sortable из набора jQuery UI*

        - **main/jquery.cookie.js** - *Плагин jQuery Cookie*

        - **book/a.take-book.js** - *Все, что касается основного функционала страницы карточки книги*

        - **book/b.eye-moov.js** - *Все, что надо, для следящего глаза*

        - **order/a.order-load.js** - *Скрипты для загрузки данных на страницу оформления заказа*

        - **order/b.verification-form.js** - *Проверки полей формы*

        - **order/c.submission-form.js** - *Отправка заказа на сервер и обработка ответов*

    - **/styles/** - *папка с less-файлами и немножечко css - но все исходники, конечно!* 

        - **about.less** - *стили для страницы "О издательстве"*

        - **book.less** - *стили для карточки книги*

        - **eye.less** - *глаз! книжный глаз! И моргает! Подключен в book.less

        - **header-footer.less** - *стили шапки и футера*

        - **home.less** - *стили главной страницы*

        - **main.less** - *основной less файл - здесь все, что касается абсолютно любой страницы, в том числе и переменные с миксинами, сюда же подключено все остальное*

        - **normalize.css** - *ну сами понимаете, если кем-то что-то уже написано..*

        - **order.less** - *стили страницы заказа*

----------
# Ход работы

##Этап I. Верстка.

*Все страницы практически сразу верстались адаптивными, не отходя от кассы. Медиазапросы прописаны методом вложенности в less. Нет каких-то определенных точек, цель - приемлемое отображение на любой ширине экрана.*

1. Верстка главной страницы. *С чего-то же надо начать.*

2. Верстка страницы "О издательстве". *Надо понимать с кем имеешь дело.*

3. Верстка карточки товара, то есть книги.

4. Глаз! Верстаем моргающий глаз!

5. Предварительная верстка страницы оформления заказа. *Т.к. среди предоставленных файлов отсутствует макет для данной страницы, то оформление выполнено на свой вкус (конечно, опираясь на прототип проекта - <https://projects.invisionapp.com/share/JX5DR38MA#/screens/123757320>*

6. Верстка весьма странного прелоадера с глазом. *Воображение было таким же больным, как и я сама*

##Этап II. Написание программной части.

###Основной функционал
1. Загрузка данных на главную страницу с сервера, добавление первых 4х книг в витрину, реальзация добавления новых книг при нажатии кнопки "Еще несколько книг".

2. Код генерации ссылок для перехода на страницу карточки книги. Организован через гет-параметр в url. Загрузка данных на страницу карточки товара. 

3. Код для загрузки страницы оформления заказа, в том числе данных о способах оплаты и доставки. Реализовано их взаимодействие. 

4. Код для проверки полей ввода формы. Проверка форм поделена на два этапа - 1) - во время заполнения формы (регулировка вводимых символов, а так же реакция на окончание заполнения поля), в основном направлена на корректность вводимых данных; 2) - после нажатия кнопки "оформить заказ" - в основном проверка на незаполненные поля.

5. Реализация отправки заказа. Обработка ответа сервера. Тестирование процесса.

###Дополнительные фичи и красоты

1. Функция реакции зрачка глаза на движение курсора на главной странице. Функция адаптивна, реагирует на изменение размеров окна браузера.

2. Подключение, настройка и написание некоторых своих функций к работе плагина Sortable для реализации перетаскивания книг в витрине. Цель - приблизить внешний вид работы плагина к заявленному макету.

3. Реализация записи данных в куки для запоминания состояния витрины главной страницы при переходе между страницами сайта. Переработка функции загрузки книг в витрину - добавлено чтение куков.

4. Реализация поиска книг на сайте, вывод результатов.

##Этап III. Тестирование, отладка, правки.

Наведение порядка, исправление ошибок и т.д. Такие правки, как добавление обработки ошибки запроса к серверу по таймауту на все страницы (при загрузке данных). Можно сказать, что этот этап длится на протяжении всей работы над проектом.