// Узнаем ID книги, которую у нас хотят купить
var bookId = window.location.search.replace('?', '');

var bookData;
var delivery;
var payment;

// Загрузим данные книги на страницу
function loadBook(data) {
	bookData = data;
	$('h2.order-for a').text(bookData.title).attr({
		'href': '../book/?' + bookId
	});

	var marker = takeMarker();
	$('.book-marker').addClass(marker);

	$('.book-cover img').attr({
		'src': bookData.cover.large
	});

	$('#priceres').text(bookData.price).data({'priceBook': bookData.price, 'priceDel': 0});

	$('.book').click(function() {
		window.location = '../book/?' + bookId;
	}).css('opacity', '1');
};

// Загурзим данные способов доставки на страницу
function getDelivery(data){
	delivery = data;

	delivery.forEach(function(elem){
		var radioBox = document.createElement('p');

		$(radioBox).html('<input class="radio" type="radio" name="delivery"><label></label>')
		.find('input').attr({
			'id': elem.id,
			'value': elem.price
		}).data('needAdress', elem.needAdress).change(function() {
			radioDelivery(this);
		});
		if (elem.price == 0){
			$(radioBox).find('label').html(elem.name + " - Бесплатно");
		}else{
			$(radioBox).find('label').html(elem.name +" - "+ elem.price + "$");
		}
		$(radioBox).find('label').attr({
			'for': elem.id
		});
		$(radioBox).appendTo('fieldset.delivery');
	});
}

// Загурзим способы оплаты на страницу
function getPayment(data){
	payment = data;
	payment.forEach(function (elem) {
		var radioBox = document.createElement('p');
		$(radioBox).html('<input class="radio" type="radio" name="payment"><label></label>')
		.find('input').attr({
			'id': elem.id,
			'value': elem.title,
			'disabled':''
		}).data('deliverys', elem.availableFor).change(function() {
			$('fieldset.payment').children('p.error').css('display', 'none');
		});
		$(radioBox).find('label').attr({
			'for': elem.id
		}).text(elem.title);
		$(radioBox).appendTo('fieldset.payment');
	});
}

// Что должно происходить при выборе способов доставки
function radioDelivery(radio){
	$('fieldset.delivery').children('p.error').css('display', 'none');
	$('.order-adress').css('display', 'none');
	if ($(radio).data('needAdress')){
		$('.order-adress').detach().insertAfter($(radio).parent('p'));
		$('.order-adress').css('display', 'block');
	}
	$('#priceres').data('priceDel', +radio.value)
	
	$('#priceres').text($('#priceres').data('priceDel') + $('#priceres').data('priceBook'));
	$('fieldset.payment input').each(function(i, el) {
		var deliverys = $(el).data('deliverys');
		if (deliverys.includes(radio.id)){
			$(el).removeAttr('disabled');
		}else{
			$(el).attr('disabled', '');
			el.checked = false;
		}
	});
}

// Попросим данные о книгах у сервера и запустим функцию обработки их
ajax(loadBook, bookId);
// Попросим данные о способах доставки у сервера и запустим функцию обработки их
$.get('https://netology-fbb-store-api.herokuapp.com/order/delivery', getDelivery);
// Попросим даннные о способах оплаты у сервера и запустим функцию обработки их
$.get('https://netology-fbb-store-api.herokuapp.com/order/payment', getPayment);
// Когда все загрузилось
$(function() {
	// Для начала поставим проверки во время ввода
	$('input[type = text][name != search]').keyup(function(e){
		$(this).prev('p.error').css('display', 'none');
		$(this).removeClass('error');
		if (this.name == 'name') {// Для ввода имени
			if (this.value.match(/[^а-яёa-z -]/ig)){
  			     this.value = this.value.replace(/[^а-яёa-z -]/ig, '');
  			     $(this).prev('p.error').css('display', 'block').text('Пожалуйста, только буквы!');
  			 }
		}else if(this.name == 'phone'){ //для ввода телефона
			if (this.value.match(/[^0-9\+ -/)/)]/g)){
				 this.value = this.value.replace(/[^0-9\+ -/)/)]/g, '');
				 $(this).prev('p.error').css('display', 'block').text('Мы понимаем номера телефонов только в цифрах!');
			}
  			if (this.value.search(/^[ -\(\)]/g) != -1){
  				this.value = this.value.replace(/^[ -\(\)]/g, function(str, offset){
  					if (offset == 0) {
  						return '';
  					}else{
  						return str;
  					}
  				});
  				// console.log('j')
  			};
  			if (this.value.search(/\+/g) != -1) {
  					this.value = this.value.replace(/\+/g, function (str, offset) {
  						if (offset == 0) {
  							return str;
  						}else{
  							return '';
  						}
  					});
  			}
		}else if (this.name == 'e-mail'){ //для ввода e-mail
			if (this.value.match(/[а-яё]/ig)){
				this.value = this.value.replace(/[а-яё]/i, '');
				$(this).prev('p.error').css('display', 'block').text('Мы не верим в существования элекронных адресов на русском!');
			}
		}
	});
	// Когда поле уже заполнено
	$('input[type = text][name != search]').change(function(e) {
		$(this).removeClass('error')
		if (this.value == '') {
			$(this).prev('p.error').css('display', 'block').text('Не проходите мимо, не оставляйте это поле пустым!');
		}else{
			if (this.name == 'phone'){
				if (!/^[\d+][\d\(\)\ -]{6,14}\d$/.test(this.value)){
					$(this).prev('p.error').css('display', 'block').text('С этим номером что-то не так.. О_о');
				}else{
					$(this).prev('p.error').css('display', 'none');
				}
			}else if(this.name == 'e-mail'){
				if(!/^[\w-\.]+@[\w-]+\.[a-z]{2,3}$/i.test(this.value)){
					$(this).prev('p.error').css('display', 'block').text('А это точно электронный адрес?');
				}else {
					$(this).prev('p.error').css('display', 'none');
				}
			}else{
				$(this).prev('p.error').css('display', 'none');
			}
		}

	});

    // блокируем нажание enter на форме
    $('form').bind('keydown', function (e) {
          if (e.keyCode === 13) {
            e.preventDefault();
            
        }
    });

	$('button.order-make').click(function(e) {
		e.preventDefault();
		var result = true;
		$('input[type = text][name != search]').each(function(index, el) {
			if (el.value == ''){
				if (el.name == 'adress' && $(el).parent('.order-adress').css('display') == 'block'){
					$(el).prev('p.error').css('display', 'block').text('А куда доставлять-то?');
					$(el).addClass('error');
					result = false;
				}else if (el.name != 'adress'){
					$(el).prev('p.error').css('display', 'block').text('Заполните это поле');
					$(el).addClass('error');
					result = false;
				}
			}else if($(el).prev('p.error').css('display') == 'block'){
				$(el).addClass('error');
				result = false;
			}
		});
		if(!$('input[name = delivery]:checked').val()){
			$('fieldset.delivery').children('p.error').css('display', 'block').text('Определитесь, пожалуйста');
			result = false;
		}
		if(!$('input[name = payment]:checked').val()){
			$('fieldset.payment').children('p.error').css('display', 'block').text('А как будем расплачиваться?');
			result = false;
		}
		if (result){
			submitForm();
		}		
	});
});function submitForm() {
	$('form.order').css('display', 'none');
	$('.preloader').css('display', 'block');
	var comment = $('fieldset.comment textarea').val();
	if (comment == ''){
		comment = "none"
	};

	 var body = {
		'manager': 'djailin.vasu@gmail.com',
		'book': bookId,
		'name': $('input[name = name]').val(),
		'phone': $('input[name = phone]').val(),
		'email': $('input[name = e-mail]').val(),
		'comment': comment,
		'delivery': {
		  'id': $('input[name = delivery]:checked')[0].id,
		  'address': $('input[name = adress]').val(),
		},
		'payment': {
		  'id': $('input[name = payment]:checked')[0].id,
		  'currency': 'R01235'
		}
	};
	$.ajax({
		url: 'https://netology-fbb-store-api.herokuapp.com/order',
		type: 'POST',
		processData: false,
		data: JSON.stringify(body),
		contentType: 'application/json',
		success: function (){
			$('.preloader').css('display', 'none');
			$('.done').css('display', 'block');
		},
		error: function (r, type) {
			$('.preloader').css('display', 'none');
			$('form.order').css('display', 'block');
			$('.error.g').css('display', 'block').text(function () {
				if (type == 'timeout') {
					return 'Кажется, наш почтовый ящик засорился. Пожалуйста, попробуйте позже.';
				}else{
					return 'Упс. Что-то пошло не так. Проверьте поля и попробуйте еще раз!';
				}
			});

		}
	});
}