
// Перо-наперво узнаем id книги, которую будем показывать
var bookId = window.location.search.replace('?', '');

var bookData;

// Функция для подставления данных на страницу карточки книги
function takeBook(data){
	bookData = data;
	// console.log(bookData);
	var marker = takeMarker();
	$('.book-marker').addClass(marker);
	$('.book-cover img').attr({
		'src': bookData.cover.large
	});
	$('.book-description').html(bookData.description);
	for (var i = 0; i < 2; i++) {
		$($('.reviews .avatar img')[i]).attr({
			'src': bookData.reviews[i].author.pic,
			'alt': bookData.reviews[i].author.name
		});
		$($('.reviews .cite')[i]).html(bookData.reviews[i].cite);
		$($('.features .avatar img')[i]).attr({
			'src': bookData.features[i].pic
		});
		$($('.features .cite')[i]).html(bookData.features[i].title);
	}
	$('button.buy span#price').text(bookData.price);
	citeHeights();
	$('.card').css('opacity', '1');
};

// Чтобы все было ровненько!
function citeHeights(){
	var heights = [];
	$(".cite").each(function(indx, element){
  		heights.push($(element).height());
	});	
	var max = Math.max.apply(0, heights);
	$(".cite").height(max);
}

ajax(takeBook, bookId);

// Когда все загрузилось
$(function(){
	$('button.buy').click(function() {
		window.location = '../order/' + "?" + bookId;
	});
	
	$('#coverimg')[0].onload = function() {
		eyeMoove();
	};
});

function eyeMoove() {
	var x0, y0, top, left, size, min, max;
	function takeSizes(param){
		 size = $('.book-eye').width();
		if (size == 100 || size < 120){
			min = 10;
			max = 70;
			top = left = 41;
		}else if(size == 120 || size < 140){
			min = 15;
			max = 90;
			top = left = 50;
		}else if(size == 140){
			min = 20;
			max = 100;
			top = left = 58;
		}
		if (param){
			$('.book-eye-pupil').css({top: top, left: left});
		}
		x0 = $('.book-eye-pupil').offset().left + ($('.book-eye-pupil').width()/2);
		y0 = $('.book-eye-pupil').offset().top + ($('.book-eye-pupil').height()/2);
	};
	takeSizes(false);
	$(window).resize(function() {
		takeSizes(true);
	});
	$(document).mousemove(function(e){
		var newTop = top + (e.pageY - y0)*0.09;
		var newLeft = left + (e.pageX - x0)*0.09;
		if (newTop < min){
			newTop = min;
		}
		if (newLeft < min){
			newLeft = min;
		}
		if (newTop > max){
			newTop = max;
		}
		if (newLeft > max){
			newLeft = max;
		}
		$('.book-eye-pupil').css({top: newTop, left: newLeft});
	});
}


