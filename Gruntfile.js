module.exports = function(grunt) {

	grunt.initConfig({
		pkg : grunt.file.readJSON('package.json'),
		less: {
			build: {
				files: {
					'result/css/main.css': 'src/styles/main.less'
				}
			}
		},
		ftp_push: {
		    demo: {
		    	options: {
		    		authKey: 'netology',
		    		host: 'university.netology.ru',
		    		dest: '/fbb-store/',
		    		port: 21
		    	},
		    	files: [{
		    		expand: true,
		    		cwd: 'result/',
		    		src: [
		    		      '**'
		    		]
		        }]
		    }
		 },
		 concat:{
		 	options: {
		 		separator:'',
		 	},
			overall:{
				src: ['src/scripts/*.js'],
		 		dest: 'result/js/script.js',
			},
		 	main:{
		 		src: ['src/scripts/main/*.js'],
		 		dest: 'result/js/main.js',
		 	},
		 	book:{
		 		src: ['src/scripts/book/*.js'],
		 		dest: 'result/js/book.js',
		 	},
		 	order:{
		 		src: ['src/scripts/order/*.js'],
		 		dest: 'result/js/order.js',
		 	}
		 },
		 autoprefixer: {
		 	options: {
		 		browsers: ['> 0.1%']
		 	},
		 	dev: {
		 		src: 'result/css/*.css'
		 	}
		 },
		 imagemin:{
		 	dynamic:{
		 		files: [{
		 			 expand: true,
          			 cwd: 'src/',
          			 src: ['**/*.{png,jpg,gif}'],
          			 dest: 'result/'
		 			}]
		 		}
		 	},
		 watch: {
		 	options: {
		 		spawn: false,
		 		livereload: true
		 	},
		 	scripts: {
		 		files:['src/scripts/*.js', 'src/scripts/*/*.js'],
		 		tasks: ['concat'],
		 		options: {
		 			livereload: true
		 		}
		 	},
		 	styles: {
		 		files: 'src/styles/*.less',
		 		tasks: ['less', 'autoprefixer'],
		 		options: {
		 			livereload: true
		 		}
		 	},
		 	html: {
		 		files: 'result/**/*.html',
		 		options: {
		 			livereload: true
		 		}
		 	}
		 }
	});

	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-ftp-push');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-autoprefixer');
	grunt.loadNpmTasks('grunt-contrib-imagemin');

	grunt.registerTask('default', ['less', 'concat', 'ftp_push']);
	grunt.registerTask('start', ['less', 'concat']);
};